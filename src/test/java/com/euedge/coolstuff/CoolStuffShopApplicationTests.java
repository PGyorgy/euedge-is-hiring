package com.euedge.coolstuff;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.data.domain.Page;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.euedge.coolstuff.business.ProductService;
import com.euedge.coolstuff.domain.Product;
import com.euedge.coolstuff.domain.filter.KosherFilter;
import com.euedge.coolstuff.domain.filter.NameFilter;
import com.euedge.coolstuff.domain.filter.ProductFilter;
import com.euedge.coolstuff.domain.filter.TagFilter;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = CoolStuffShopApplication.class)
public class CoolStuffShopApplicationTests {

	private static final String SEARCH_STRING = "Cool";
	private static final String SEARCH_STRING_2 = "away with";
	private static final String NAME_FILTER = "Cool Melon";
	private static final String TAG_FILTER = "fruit";	
	
    @Autowired
    private ProductService productService;
 
	@Test
	public void contextLoads() {
		List<Product> result = productService.loadAllProducts();
		assertNotNull(result);
		assertEquals(8, result.size());
	}
	
	@Test
	public void searchTest(){
		Page<Product> result = productService.searchByString(SEARCH_STRING);
		assertNotNull(result);
		assertEquals(3, result.getNumberOfElements());
			
		Page<Product> result2 = productService.searchByString(SEARCH_STRING_2);
		assertNotNull(result2);
		assertEquals(2, result2.getNumberOfElements());
	}
	
	@Test
	public void filterTest(){
		
		ProductFilter nameFilter = new NameFilter();
		nameFilter.setFilter(NAME_FILTER);
		
		ProductFilter tagFilter = new TagFilter();
		tagFilter.setFilter(TAG_FILTER);
		
		ProductFilter kosherFilter = new KosherFilter();
		
		List<ProductFilter> filerList = new ArrayList<ProductFilter>(Arrays.asList(nameFilter, tagFilter, kosherFilter));
		
		List<Product> result = productService.filterProducts(filerList);
		assertNotNull(result);
		assertEquals(1, result.size());
		
	}
	
}
