package com.euedge.coolstuff.web;

import java.util.List;
import org.apache.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.euedge.coolstuff.business.ProductService;
import com.euedge.coolstuff.domain.Product;
import com.euedge.coolstuff.domain.filter.ProductFilter;

@RestController
@RequestMapping("/api/product")
public class ProductController {
	
	/**
	 * Unfortunately I couldn't understand every task
	 * It is not clear for me what you mean filter and search. 
	 * The difference is not clear for me and it means that it is not clear how to merge them. 
	 * 
	 * What I understand: 
	 * 
	 * I must implement two searching logic.
	 * First : based on different fields of the product 
	 * Second : based on a string which checks more field of product
	 * I must use paging and sorting
	 * 
	 * I have implemented examples for them and I have added tests
	 *  
	 */

	private static final Logger logger = Logger.getLogger(ProductController.class);

    @Autowired
    private ProductService productService;

    @RequestMapping
    public List<Product> loadAllProducts() {
        return productService.loadAllProducts();
    }

    @RequestMapping(path = "filter", method = RequestMethod.POST)
    public List<Product> filterProducts(@RequestBody List<ProductFilter> filters) {
    	logger.info("Received filter request");
        return productService.filterProducts(filters);
    }

    @RequestMapping(path = "search/{searchTerm}", method = RequestMethod.POST)
    public List<Product> search(@PathVariable String searchTerm) {
    	logger.info("Received search request: "+searchTerm);
        return productService.searchByString(searchTerm).getContent();
    }

}
