package com.euedge.coolstuff.domain.filter;

import javax.validation.constraints.NotNull;

import com.euedge.coolstuff.domain.Product;

public class TagFilter implements ProductFilter{
	
	@NotNull
	private String tag;

	@Override
	public boolean apply(Product product) {
		return product.getProductTag().stream().anyMatch(p -> p.getName().equals(tag));
	}
	
	@Override
	public void setFilter(String filter) {
		this.tag = filter;
	}

}
