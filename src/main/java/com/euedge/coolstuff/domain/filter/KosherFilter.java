package com.euedge.coolstuff.domain.filter;

import org.springframework.jmx.access.InvalidInvocationException;

import com.euedge.coolstuff.domain.Product;

public class KosherFilter implements ProductFilter{
	
	@Override
	public boolean apply(Product product) {
		return product.isKosher();
	}

	@Override
	public void setFilter(String filter) {
		throw new InvalidInvocationException("");
	}

}
