package com.euedge.coolstuff.domain.filter;

import javax.validation.constraints.NotNull;

import com.euedge.coolstuff.domain.Product;

public class NameFilter implements ProductFilter {

	@NotNull
	private String name;
	
    @Override
    public boolean apply(Product product) {
        return name.equals(product.getName());
    }

	@Override
	public void setFilter(String filter) {
		this.name = filter;
	}
    
}
