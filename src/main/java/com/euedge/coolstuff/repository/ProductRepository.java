package com.euedge.coolstuff.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.euedge.coolstuff.domain.Product;

@Repository
public interface ProductRepository extends CrudRepository<Product, Long>, JpaSpecificationExecutor {

	@Override
	List<Product> findAll();
	
	Page<Product> findByNameLikeOrDescriptionLike(String term, Pageable page);
}
