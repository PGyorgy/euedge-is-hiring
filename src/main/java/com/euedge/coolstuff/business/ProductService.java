package com.euedge.coolstuff.business;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.euedge.coolstuff.domain.Product;
import com.euedge.coolstuff.domain.Product_;
import com.euedge.coolstuff.domain.ProductTag_;
import com.euedge.coolstuff.domain.filter.ProductFilter;
import com.euedge.coolstuff.repository.ProductRepository;

@Service
public class ProductService {

	private static final int PAGE = 0;
	private static final int PAGE_SIZE = 5;
	
	@Autowired
	private ProductRepository repository;

	public List<Product> loadAllProducts() {
		return repository.findAll();
	}

	public List<Product> filterProducts(List<ProductFilter> filters) {
		List<Product> allProducts = loadAllProducts();
		List<Product> filterPassedProducts = new ArrayList<>();
		for (Product product : allProducts) {
			boolean filtered = false;
			// TODO: implement product field specific filters
			for (ProductFilter filter : filters) {
				if (!filter.apply(product)) {
					filtered = true;
					break;
				}
			}
			if (!filtered) {
				filterPassedProducts.add(product);
			}
		}
		return filterPassedProducts;
	}
	
	public Page<Product> searchByString(String searchTerm){
		

		if (StringUtils.isEmpty(searchTerm)){
			throw new InvalidParameterException("Search term is empty");
		}		
		
		Page<Product> result = repository.findAll(createSpecification(searchTerm), createPageRequest());
		
		return result;
	}
	
	private Specification<Product> createSpecification(String searchTerm){		
		return (root, query, cb) -> {						
            return cb.or(
                    cb.like(cb.lower(root.<String>get(Product_.name)), "%"+searchTerm.toLowerCase()+"%"),
                    cb.like(cb.lower(root.<String>get(Product_.description)), "%"+searchTerm.toLowerCase()+"%")
            );
        };
	}	
	
	private Pageable createPageRequest() {
		return new PageRequest(PAGE, 
								PAGE_SIZE, 
								new Sort(Sort.Direction.ASC, "name", "price")
	    );
	    
    }
}
